/*Team Member Names & Student ID:
Brighton Tan - S10185160
Glenn - S10195629B
Team: 11
Class: P05*/

#include <iostream>
#include "Bus.h"
#include "BusStop.h"
#include "Dictionary.h"
#include "List.h"
#include "BusRoute.h"
#include "Database.h"
struct BusStopDist;
using namespace std;

/*Option 1: Create new item (Bus Stop / Bus / Route) [Glen]
* 
  The function is a create function which enables users to create a new object
  of their desire (Bus, BusRoute or BusStop)

  The basic inputs prompted will be the attributes that the class contains.
  Class-dependent attributes will be set to NULL until further alterations.

  After creation, the new object will be added to their respective dictionaries.
  The output will be a prompt indicating that the creation was successful
*/
void createNewItem(Dictionary* busDict, Dictionary* busRouteDict, Dictionary* busStopDict) {
	string option;
	cout << "---Select the item you would like to create---" << '\n' << endl;
	cout << "1. Bus" << endl;
	cout << "2. Bus Route" << endl;
	cout << "3. Bus Stop" << endl;
	cout << endl;
	cout << "-----------------------------------------------" << '\n' << endl;
	cout << "Enter option: ";
	cin >> option;
	cout << endl;

	if (option == "1") {
		cout << "Creating a new bus..." << '\n' << endl;
		string bNo, lP, m;
		BusStop* nS = NULL;
		double dT = 0.0;
		int yMf;
		cout << "Enter the new Bus Number: ";
		cin >> bNo;
		cout << endl;
		cout << "Enter the bus License Plate: ";
		cin >> lP;
		cout << endl;
		Bus* busValidation = (Bus*)(busDict->get(lP));
		while (busValidation != NULL) {
			cout << "Bus Number Plate already exists! Please enter a valid Number Plate: ";
			cin >> lP;
			cout << endl;
			busValidation = (Bus*)(busDict->get(lP));
		}
		cout << "Enter the bus Model: ";
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		getline(cin, m);
		cout << endl;
		cout << "Enter the Year Manufactured: ";
		cin >> yMf;
		cout << endl;
		Bus* b = new Bus(bNo, nS, dT, lP, m, yMf);
		busDict->add(lP, b);
		cout << "New Bus successfully created! Returning to Main Menu..." << "\n" << endl;
	}

	else if (option == "2") {
		cout << "Creating a new Bus Route..." << "\n" << endl;
		string bNo;
		cout << "Enter the bus route Number: ";
		cin >> bNo;
		BusRoute* brValidation = (BusRoute*)(busRouteDict->get(bNo));
		cout << endl;
		while (brValidation != NULL) {
			cout << "Bus Route already exists! Please enter a valid bus route number: ";
			cin >> bNo;
			cout << endl;
			brValidation = (BusRoute*)(busRouteDict->get(bNo));
		}
		BusRoute* br = new BusRoute(bNo);
		busRouteDict->add(bNo, br);
		cout << "New Bus Route successfully created! Returning to Main Menu..." << "\n" << endl;

	}

	else if (option == "3") {
		cout << "Creating a new Bus Stop..." << "\n" << endl;
		string bsName;
		string bsL;
		cout << "Enter the name of the Bus Stop: ";
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		getline(cin, bsName);
		cout << endl;
		BusStop* bsValidation = (BusStop*)(busStopDict->get(bsName));
		while (bsValidation != NULL) {
			cout << "Bus Stop already exists! Please enter a valid Bus Stop Name: ";
			getline(cin, bsName);
			cout << endl;
			bsValidation = (BusStop*)(busStopDict->get(bsName));
		}
		cout << "Enter the location of the Bus Stop: ";
		getline(cin, bsL);
		BusStop* bs = new BusStop(bsName, bsL);
		busStopDict->add(bsName, bs);
		cout << "" << endl;
		cout << "New Bus Stop successfully created! Returning to Main Menu..." << "\n" << endl;
	}
	else {
		cout << "Invalid Input. Returning to Main Menu..." << '\n' << endl;
	}
}

/*Option 2: Display all bus routes [Brighton]
* 
  This function is a printing function which does not accept inputs

  It uses the Dictionary's print() function which recurs through
  the dictionary and traverses through the linked list nodes.

  It then displays every node's item as an output.*/
void displayAllBusRoutes(Dictionary* busRouteDict) {
	cout << "All available bus routes" << endl;
	cout << endl;
	busRouteDict->print("BusRoute");
}

/*Option 3: Display details of route [Glen]
  This function is a printing function that displays the details of the route (The List of bus stops that is under the route)

  The function asks for the bus number from the user.
  After retrieving the bus number, it will be used as a key for the hash
  function and the route object will be retrieved.

  The doubly linked list attribute will be printed out as output.*/
void displayRouteDetails(Dictionary* busRouteDict) {
	string bNo;
	cout << "Enter a route number: ";
	cin >> bNo;
	cout << endl;

	BusRoute* busRoute = (BusRoute*)(busRouteDict->get(bNo));
	if (busRoute != NULL)
	{
		cout << "Here are the bus stops currently in route " << bNo << '\n' << endl;

		List* busStopList = busRoute->getBusStopLinkedList();
		busStopList->print("busRoute");

		cout << '\n' << "End of List" << '\n' << endl;
	}
	else
	{
		cout << "No bus route registered under bus number..." << '\n' << endl;
	}
}

/*Sub-function for option 4: Inserting a Bus Stop to Route

  This is an insert function for inserting a new bus stop into a specific bus route
  It receives a bus stop name as input and first ensures that the bus stop is
  not already a part of it.

  It then inserts it into the bus stop linked list attribute of the bus route

  A sorted insert is implemented to ensure that the distance
  between the starting point and the bus stop is properly calculated.

  If the bus stop does not exist, a new one will be created, prompting user for its location.
  A text output stating that the insert was successful will be displayed.*/
void insertBusStopToRoute(Dictionary* busRouteDict, Dictionary* busStopDict, BusRoute* busRoute) {
	string busName;
	cout << "Please enter the name of the bus stop you would like to insert: ";
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(cin, busName);
	cout << endl;

	BusStop* selectedBusStop = (BusStop*)(busStopDict->get(busName));

	if (selectedBusStop == NULL) {
		char choice;
		while (true) {
			cout << "Specified bus stop does not exist. Would you like to create a new bus stop? (Y/N): ";
			cin >> choice;
			cout << endl;

			if (toupper(choice) == 'Y')
			{
				string bsL;
				cout << "Enter the location of the Bus Stop: ";
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
				getline(cin, bsL);
				cout << endl;
				selectedBusStop = new BusStop(busName, bsL);
				busStopDict->add(busName, selectedBusStop);
				cout << "New Bus Stop successfully created!" << "\n" << endl;
				break;
			}
			else if (toupper(choice) == 'N') {
				cout << "Returning to menu..." << endl;
				cout << endl;
				return;
			}
			else {
				cout << "Invalid option, please enter a valid option (Y/N)." << endl;
				cout << endl;
			}
		}
	}

	double dist;
	cout << "Please first enter the distance of this bus stop relative to the route (km) : ";
	cin >> dist;
	cout << endl;

	BusStopDist* bsd = new BusStopDist;
	bsd->busStop = selectedBusStop;
	bsd->distance = dist;

	List* busStopList = busRoute->getBusStopLinkedList();
	busStopList->SortedInsert(bsd);

	cout << "Successfully Inserted the bus stop at the appropriate index of the bus route." << endl;
	cout << endl;

	busStopList->print("busRoute");
	cout << endl;

	return;
}

/*Sub-function for option 4: Deleting a bus stop from route

  The function is a delete function that removes a bus stop from the
  route's bus stop linked list.

  The bus stop linked list is first displayed in indexes.
  The input will be the index number of the bus stop to be removed

  The List's remove() function is called to remove the specified bus stop
  and a text indicating successful removal is displayed as output.*/
void deleteBusStopFromRoute(Dictionary* busRouteDict, Dictionary* busStopDict, BusRoute* busRoute) {
	List* busStopList = busRoute->getBusStopLinkedList();
	busStopList->print("busRoute");
	cout << endl;

	string busStopIndex;
	cout << "Please enter the index number of the bus stop you would like to remove (Type 'Exit' to return): ";
	cin >> busStopIndex;
	cout << endl;

	if (busStopIndex == "Exit") {
		return;
	}
	else {
		int index = stoi(busStopIndex);
		busStopList->remove(index);
		cout << "Bus Stop successfully removed." << endl;
		cout << endl;
	}
}

/*Sub-function for option 4: Deleting the entire selected route

  This is a delete function that removes a specific bus route from the dictionary

  This function prompts for a Yes or No input to indicate its removal
  The function makes use of the List's deconstructor to remove its linked list attribute

  The BusRoute object will be removed from the dictionary and
  a text displaying its successful removal will be displayed.*/
bool deleteEntireRoute(Dictionary* busRouteDict, BusRoute* busRoute, string route) {
	char choice;
	while (true) {
		cout << "Are you sure you want to delete the entire route (Current Route: " << route << " )? (Y/N): ";
		cin >> choice;
		cout << endl;

		if (toupper(choice) == 'Y')
		{
			List* busStopList = busRoute->getBusStopLinkedList();
			busRouteDict->remove(route);
			busStopList->~List();
			delete(busRoute);
			busRoute = NULL;
			cout << "Bus route successfully deleted. Returning to main menu..." << endl;
			cout << endl;
			return true;
		}
		else if (toupper(choice) == 'N') {
			return false;
		}
		else {
			cout << "Invalid option, please try again. " << endl;
			cout << endl;
		}
	}

}

/*Option 4: Alter Route (Search/Insert/Delete) [Brighton]

  This function is an update/delete function which updates the attributes of a route or deletes the route itself

  The bus route number is prompted as input and retrieved from the bus route dictionary.

  The user will be presented with an option to either
  Insert a bus stop, Delete a bus stop from the route or delete the whole route.

  This function acts as a menu interface and uses other functions for completing its task.*/
void alterRoute(Dictionary* busRouteDict, Dictionary* busStopDict) {
	while (true) {

		string route;
		cout << "Please enter the route you would like to alter (Type 0 to exit): ";
		cin >> route;
		cout << endl;

		BusRoute* busRoute;

		if (route == "0") {
			break;
		}
		else {
			busRoute = (BusRoute*)(busRouteDict->get(route));
		}

		if (busRoute != NULL)
		{
			cout << "Here are the bus stops currently in route " << route << '\n' << endl;

			List* busStopList = busRoute->getBusStopLinkedList();
			busStopList->print("busRoute");
			cout << endl;

		}
		else
		{
			cout << "No such bus route found, please try again." << '\n' << endl;
			return alterRoute(busRouteDict, busStopDict);
		}

		while (true) {
			string option;
			cout << "Please select what you would like to do with the route (Current Route: " << route << " ):" << '\n' << endl;
			cout << "1. Insert a bus stop to the route" << endl;
			cout << "2. Delete a bus stop from the route" << endl;
			cout << "3. Delete the entire route" << endl;
			cout << "4. Back to route selection" << endl;
			cout << "5. Exit to main menu" << endl;
			cout << "-------------------------------------------------------" << '\n' << endl;
			cout << "Enter option: ";
			cin >> option;
			cout << endl;

			if (option == "1") {
				insertBusStopToRoute(busRouteDict, busStopDict, busRoute);
			}
			else if (option == "2") {
				deleteBusStopFromRoute(busRouteDict, busStopDict, busRoute);
			}
			else if (option == "3") {
				bool success = deleteEntireRoute(busRouteDict, busRoute, route);
				if (success) {
					break;
				}
			}
			else if (option == "4") {
				return alterRoute(busRouteDict, busStopDict);
			}
			else if (option == "5") {
				cout << "Returning to main menu..." << endl;
				cout << endl;
				break;
			}
			else {
				cout << "Invalid Option, please try again." << endl;
				cout << endl;
			}
		}

		break;
	}

}

/*Option 5: Alter Buses (Edit/Delete) [Brighton]

  This function is an update/delete function which changes the attribtues of the bus object or deletes the bus object itself.

  The program first prompts user for a bus number plate
  The specific bus object will be retrieved from the bus dictionary

  The system will then display a menu, giving the user a list of actions for the object.
  The bus class set functions are called to update the attribute data and in the case of delete, the bus is removed
  from the dictionary and then deleted and set as NULL.
  
  A success message will be displayed upon completion of action*/

void alterBus(Dictionary* busDict, Dictionary* busStopDict) {
	while (true) {
		string licenseNo;
		cout << "Please enter the bus license plate of the bus you would like to alter (Type 0 to exit): ";
		cin >> licenseNo;
		cout << endl;

		Bus* selectedBus;

		if (licenseNo == "0") {
			break;
		}
		else {
			selectedBus = (Bus*)(busDict->get(licenseNo));
		}

		if (selectedBus != NULL)
		{
			cout << "Bus Number: " << selectedBus->getBusNo() << '\n' << endl;
			cout << "Bus Model: " << selectedBus->getModel() << '\n' << endl;
			cout << "Bus Manufactured Year: " << selectedBus->getYearManufactured() << '\n' << endl;
			cout << "Bus License Plate: " << selectedBus->getLicensePlate() << '\n' << endl;

			while (true) {
				string option;
				cout << "Please select what you like to do with the bus (License plate: " << selectedBus->getLicensePlate() << ") ?" << endl;
				cout << endl;
				cout << "1. Edit Bus Number" << endl;
				cout << "2. Edit Bus Next Stop" << endl;
				cout << "3. Edit Distance Travelled" << endl;
				cout << "4. Delete Bus" << endl;
				cout << "5. Back to bus selection" << endl;
				cout << "6. Exit to main menu" << endl;
				cout << "------------------------------------------------" << '\n' << endl;
				cout << "Enter option: ";
				cin >> option;
				cout << endl;

				if (option == "1") {
					string busNo;
					cout << "Enter new bus number: ";
					cin >> busNo;
					cout << endl;
					selectedBus->setBusNo(busNo);

					cout << "Bus number successfully edited." << endl;
					cout << endl;
				}
				else if (option == "2") {
					string nextStop;
					cout << "Enter the name of next stop: ";
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
					getline(cin, nextStop);
					cout << endl;

					BusStop* busStop = (BusStop*)busStopDict->get(nextStop);

					if (busStop == NULL) {
						cout << "No such bus stop found. Unable to set next stop." << endl;
						cout << endl;
					}
					else {
						selectedBus->setNextStop(busStop);
						cout << "Bus next stop successfully edited." << endl;
						cout << endl;
					}
				}
				else if (option == "3") {
					double distanceTravelled;
					cout << "Enter distance travelled: ";
					cin >> distanceTravelled;
					cout << endl;
					selectedBus->setDistanceTravelled(distanceTravelled);

					cout << "Distance travelled successfully edited." << endl;
					cout << endl;
				}
				else if (option == "4") {
					bool success = false;
					char choice;
					while (true) {
						cout << "Are you sure you want to delete the bus? (License plate: " << selectedBus->getLicensePlate() << ") ? (Y/N): ";
						cin >> choice;
						cout << endl;

						if (toupper(choice) == 'Y') {
							busDict->remove(licenseNo);
							delete(selectedBus);
							selectedBus = NULL;

							cout << "Selected Bus Successfully deleted." << endl;
							cout << endl;
							cout << "Returning to main manu..." << endl;
							cout << endl;

							success = true;
							break;
						}
						else if (toupper(choice) == 'N') {
							break;
						}
						else {
							cout << "Invalid Option, please try again." << endl;
							cout << endl;
						}
					}

					if (success) {
						break;
					}
				}
				else if (option == "5") {
					return (alterBus(busDict, busStopDict));
				}
				else if (option == "6") {
					cout << "Returning to main menu..." << endl;
					cout << endl;
					break;
				}
				else {
					cout << "Invalid Option, please try again." << endl;
					cout << endl;
				}
			}
		}
		else
		{
			cout << "No bus with such license found. Please try again" << '\n' << endl;
			return alterBus(busDict, busStopDict);
		}

		break;
	}
}

/*Option 6: Alter Bus Stops (Edit/Delete) [Brighton]

  This function is an update/delete function for changing attribtues of a
  specific bus stop or for deleting the bus stop itself.

  The user will be prompted to enter the name of the specific bus stop.
  The item will then be retrieved from the dictionary
  The system will display a menu of actions the user may perform on the object.

  The BusStop set() function is used to update the dictionary attributes. For deletion, 
  the BusStop is first checked whether it exists in a route, if it doesn't, it is 
  then removed from the dictionary, deleted and set as NULL.
  
  A success message is then displayed upon completion of function*/

void alterBusStop(Dictionary* busStopDict, Dictionary* busRouteDict) {
	while (true) {
		string busStopName;
		cout << "Please enter the name of the bus stop you would like to alter (Type 0 to exit): ";
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		getline(cin, busStopName);
		cout << endl;

		BusStop* busStop;

		if (busStopName == "0") {
			break;
		}
		else {
			busStop = (BusStop*)busStopDict->get(busStopName);
		}

		if (busStop != NULL) {
			cout << "Bus Stop Name: " << busStop->getBusStopName() << '\n' << endl;
			cout << "Bus Stop Location: " << busStop->getLocation() << '\n' << endl;

			while (true) {
				string option;
				cout << "Please select what you like to do with the bus (Bus Stop Name: " << busStop->getBusStopName() << ") ?" << endl;
				cout << endl;
				cout << "1. Edit bus Stop Name" << endl;
				cout << "2. Edit bus Stop Location" << endl;
				cout << "3. Delete bus Stop" << endl;
				cout << "4. Back to bus stop selection" << endl;
				cout << "5. Exit to main menu" << endl;
				cout << "------------------------------------------------" << '\n' << endl;
				cout << "Enter option: ";
				cin >> option;
				cout << endl;

				if (option == "1") {
					string newBusStopName;
					cout << "Enter new bus stop name: ";
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
					getline(cin, newBusStopName);
					cout << endl;
					busStop->setBusStopName(newBusStopName);

					cout << "Bus stop name successfully edited." << endl;
					cout << endl;
				}

				else if (option == "2") {
					string newBusStopLocation;
					cout << "Enter new bus stop location: ";
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
					getline(cin, newBusStopLocation);
					cout << endl;
					busStop->setLocation(newBusStopLocation);

					cout << "Bus stop location successfully edited." << endl;
					cout << endl;
				}

				else if (option == "3") {
					bool success = false;
					char choice;
					while (true) {
						cout << "Are you sure you want to delete the bus stop? (Bus Stop Name: " << busStop->getBusStopName() << ") ? (Y/N)";
						cin >> choice;
						cout << endl;

						if (toupper(choice) == 'Y') {

							bool isInRoute = busRouteDict->printBusRouteBS("BusRoute", busStop->getBusStopName());

							if (isInRoute == false) {
								busStopDict->remove(busStopName);
								delete(busStop);
								busStop = NULL;

								cout << "Bus stop Successfully deleted." << endl;
								cout << endl;
								cout << "Returning to main manu..." << endl;
								cout << endl;

								success = true;
								break;
							}
							else {
								cout << "Bus Stop currently exists in the above routes" << endl;
								cout << endl;
								cout << "Please ensure that the bus stop is removed from above routes before deleting" << endl;
								cout << endl;
								break;
							}
						}
						else if (toupper(choice) == 'N') {
							break;
						}
						else {
							cout << "Invalid Option, please try again." << endl;
							cout << endl;
						}
					}
					if (success) {
						break;
					}
				}

				else if (option == "4") {
					return alterBusStop(busStopDict, busRouteDict);
				}

				else if (option == "5") {
					cout << "Returning to main menu..." << endl;
					cout << endl;
					break;
				}
				else {
					cout << "Invalid option. Please try again" << endl;
				}
			}

		}
		else {
			cout << "No such bus stop found." << '\n' << endl;
			break;
		}

		break;
	}
}


/*Option 7: Display a particular bus location [Brighton]
* 
  This function is meant for displaying the next bus stop for
  a specific bus object.

  The user will be prompted with a license plate number

  The system then goes through the dictionary and retrieves the bus object.

  The nextStop attribute of the object will be displayed as output.*/

void displayBusLocation(Dictionary* busDict) {
	string license;
	cout << "Enter a bus license plate number: ";
	cin >> license;
	cout << endl;

	Bus* bus = (Bus*)(busDict->get(license));

	if (bus == NULL) {
		cout << "License plate does not exist." << endl;
		cout << endl;
	}
	else {
		BusStop* nextStop = bus->getNextStop();
		if (nextStop != NULL) {
			cout << "The next stop for bus " << license << " is " << bus->getNextStop()->getBusStopName() << endl;
			cout << endl;
		}
		else {
			cout << "This bus is currently not in service." << endl;
			cout << endl;
		}
	}
}


/*Sub-function used for option 8
This function takes remainingDistance as a parameter and calculates remainingTime via a formula

It then returns the double Time back to option 8*/

double calculateTime(double remainingDistance) {
	//Formula to calculate remainingTime
	return (remainingDistance * 0.2 + 0.3 * remainingDistance);
}

/*Option 8: Display bus's estimated time to reach next stop [Brighton]

  This function displays the estimated time a specific bus will take to reach a specific bus stop.

  It prompts for the bus' license plate number
  It then prompts for a specific bus stop name.

  Validation will ensure that the bus stop is part of the specific bus route.

  It then subtracts the total distance with the distance travelled and
  runs calculateTime function to calculate the time left for arrival.

  The time calculated will be printed out.*/

void displayEstimation(Dictionary* busDict, Dictionary* busRouteDict) {
	while (true) {
		string license;
		cout << "Enter a bus license plate number (Type 0 to exit): ";
		cin >> license;
		cout << endl;

		if (license == "0") {
			cout << "Returning to main menu..." << endl;
			cout << endl;
			break;
		}

		Bus* bus = (Bus*)(busDict->get(license));

		if (bus == NULL) {
			cout << "License plate does not exist. Please try again." << endl;
			cout << endl;
		}
		else {
			cout << "Bus Number: " << bus->getBusNo() << endl;
			cout << endl;
			BusRoute* r = (BusRoute*)busRouteDict->get(bus->getBusNo());
			string targetBusStopName;
			cout << "Enter target bus stop name: ";
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			getline(cin, targetBusStopName);
			cout << endl;

			//Get chosen bus stop structure
			BusStopDist* selectedBusStop = r->getBusStopLinkedList()->exists(targetBusStopName, "BusRoute");

			if (selectedBusStop != NULL) {
				//Calculate remaining distance
				double remainingDistance = selectedBusStop->distance - bus->getDistanceTravelled();

				if (remainingDistance < 0) {
					cout << "The bus has already passed the specified stop." << endl;
					cout << endl;
				}
				else {
					cout << "Remaing distance to specified stop: " << remainingDistance << "km" << endl;
					cout << endl;
					cout << "The bus will arrive at the specified stop in: " << calculateTime(remainingDistance) << " min(s)." << endl;
					cout << endl;
				}
			}
			else {
				cout << "The bus does not go to specified bus stop." << endl;
				cout << endl;
			}

			break;
		}
	}
}


/*Option 9: Display buses on a particular bus route [Glen]

  This function displays the total number of buses that are
  currently embarked on their assigned route.

  The program asks for a bus number as an input.

  A custom-made Dictionary function is called which
  traverses through the Bus Dictionary and counts the number of specified
  buses who has a distanceTravelled attribute greater than 0.

  The output is an integer indicating the number of buses*/
void displayBusesOnRoute(Dictionary* busDict)
{
	string bNo;
	cout << "Enter a route number: ";
	cin >> bNo;
	cout << endl;
	int noOfBus = busDict->printCounter(bNo, "busDict");
	
	if (noOfBus != 0) {
		cout << "There are a total of " << noOfBus << " buses on the " << bNo << " route." << '\n' << endl;
	}
	else {
		cout << "No buses are found on specified route" << endl;
		cout << endl;
	}
}


/*Option 10: Check routes that have a particular bus stop [Glen]

  This function displays the bus routes that contains a specific bus stop
  The program accepts an existing Bus Stop name as input

  It recurs through the bus route dictionary and retrieves the bus stop linked list
  It traverses through every linked list and finds the bus stop with the specified name

  If the bus stop is found, the route number is displayed in a list format as output.*/
void displayRouteBusStops(Dictionary* busRouteDict)
{
	string targetBusStopName;
	cout << "Enter a bus stop name: ";
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(cin, targetBusStopName);
	cout << endl;
	cout << "Routes that contain " << targetBusStopName << '\n' << endl;
	bool success;
	success = busRouteDict->printBusRouteBS("BusRoute", targetBusStopName);
	if (success == false) {
		cout << "No routes contain the entered bus stop." << endl;
		cout << endl;
	}
}


/*Main Program*/
int main()
{
	Dictionary* busDict = new Dictionary();
	Dictionary* busRouteDict = new Dictionary();
	Dictionary* busStopDict = new Dictionary();
	Database::createDB(busDict,busRouteDict,busStopDict);

	while (true){
		cout << "-----Bus Management Menu----------" << endl;
		cout << endl;
		cout << "1. Create a new object" << endl;
		cout << "2. Display all bus routes" << endl;
		cout << "3. Display details of a route" << endl;
		cout << "4. Alter Routes" << endl;
		cout << "5. Alter Buses  " << endl;
		cout << "6. Alter Bus Stops" << endl;
		cout << "7. Display a bus's next stop" << endl;
		cout << "8. Display a bus's estimated time to reach a certain stop" << endl;
		cout << "9. Display number of buses on a particular bus route" << endl;
		cout << "10. Check routes that have a particular bus stop" << endl;
		cout << endl;
		cout << "0. Exit " << endl;
		cout << endl;
		cout << "----------------------------------" << endl;
		cout << endl;
		cout << "Select Option: ";
		string option;
		cin >> option;
		cout << endl;

		if (option == "1")
		{
			createNewItem(busDict, busRouteDict, busStopDict);
		}

		else if (option == "2") {
			displayAllBusRoutes(busRouteDict);
		}

		else if (option == "3") {
			displayRouteDetails(busRouteDict);
		}

		else if (option == "4") {
			alterRoute(busRouteDict, busStopDict);
		}

		else if (option == "5") {
			alterBus(busDict, busStopDict);
		}

		else if (option == "6") {
			alterBusStop(busStopDict, busRouteDict);
		}

		else if (option == "7") {
			displayBusLocation(busDict);
		}

		else if (option == "8") {
			displayEstimation(busDict, busRouteDict);
		}

		else if (option == "9") {
			displayBusesOnRoute(busDict);
		}

		else if (option == "10") {
			displayRouteBusStops(busRouteDict);
		}

		else if (option == "0") {
			cout << "Closing program..." << endl;
			break;
		}

		else {
			cout << "Invalid option, please try again." << endl;
			cout << endl;
		}
	}
}





