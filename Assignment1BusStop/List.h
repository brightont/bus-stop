/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#pragma once
#include<string>
#include<iostream>
#include "BusRoute.h"
struct BusStopDist;

using namespace std;

typedef void* ItemType;

class List
{
private:
	struct Node
	{
		ItemType item;	// data item
		Node* next;	// pointer pointing to next item
		Node* previous;
	};

	Node* firstNode;	// point to the first item
	Node* lastNode;
	int  size;			// number of items in the list

public:
	List();			// constructor

	~List();		// destructor

	// add an item to the back of the list (append)
	// pre : size < MAX_SIZE
	// post: item is added to the back of the list
	//       size of list is increased by 1
	bool add(ItemType item);

	// add an item at a specified position in the list (insert)
	// pre : 0 <= index <= size
	// post: item is added to the specified position in the list
	//       items after the position are shifted back by 1 position
	//       size of list is increased by 1
	bool add(int index, ItemType item);

	// remove an item at a specified position in the list
	// pre : 0 <= index < size
	// post: item is removed the specified position in the list
	//       items after the position are shifted forward by 1 position
	//       size of list is decreased by 1
	void remove(int index);

	// get an item at a specified position of the list (retrieve)
	// pre : 0 <= index < size
	// post: none
	// return the item in the specified index of the list
	ItemType getTop();

	// check if the list is empty
	// pre : none
	// post: none
	// return true if the list is empty; otherwise returns false
	bool isEmpty();

	// check the size of the list
	// pre : none
	// post: none
	// return the number of items in the list
	int getLength();

	//------------------- Other useful functions -----------------

	// display the items in the list
	void print(string type);

	// void replace(int index, ItemType item);
	// int search(ItemType item);
	void* listAsArray();

	BusStopDist* exists(string busName, string type);

	void SortedInsert(BusStopDist* bsd);

};



