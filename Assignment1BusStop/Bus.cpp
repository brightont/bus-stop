/*Name & Student ID:
Glenn - S10195629B
Team: 11
Class: P05*/

#include "Bus.h"
#include <string>
using namespace std;

Bus::Bus(string bNo, BusStop* nSt, double dT, string lP, string m, int yMF)
{
	busNo = bNo;
	nextStop = nSt;
	distanceTravelled = dT;
	licensePlate = lP;
	model = m;
	yearManufactured = yMF;
}


void Bus::setBusNo(string b) { busNo = b; }

string Bus::getBusNo() { return busNo; }

void Bus::setNextStop(BusStop* nSt) { nextStop = nSt; }

BusStop* Bus::getNextStop() {
	return nextStop;
}

void Bus::setDistanceTravelled(double d) { distanceTravelled = d; }

double Bus::getDistanceTravelled() { return distanceTravelled; }

void Bus::setLicensePlate(string l) { licensePlate = l; }

string Bus::getLicensePlate() { return licensePlate; }

void Bus::setModel(string m) { model = m; }

string Bus::getModel() { return model; }

void Bus::setYearManufactured(int y) { yearManufactured = y; }

int Bus::getYearManufactured() { return yearManufactured; }
