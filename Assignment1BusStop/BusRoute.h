/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#pragma once
using namespace std;
#include<string>
#include "List.h"
#include "BusStop.h"
#include<iostream>
class List;
class BusStop;

struct BusStopDist {
	double distance;
	BusStop* busStop;
};

class BusRoute
{
private:
	List* dLinkedListBusStop;
	string busNo;
	double maxDistance;

public:
	BusRoute(string bNo); //Constructor
	void setBusNo(string b);
	string getBusNo();
	void addToBusRoute(BusStopDist* busStop);
	List* getBusStopLinkedList();
	void setMaxDistance(double md);
	double getMaxDistance();
	void addToBusRouteIndex(int index, BusStopDist* bStop);
};

