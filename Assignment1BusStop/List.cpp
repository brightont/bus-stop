/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#include <string>
#include <iostream>
#include <iomanip>
#include "List.h"
#include "BusStop.h"
struct busStopDist;

using namespace std;

//Constructor
List::List()
{
	firstNode = NULL;
	size = 0;
}
//Deconstructor
List::~List()
{
	Node* current = firstNode; //Current pointer becomes first node pointer

	while (current != NULL)
	{
		Node* tempNode = current;
		current = current->next; //Keep traversing until current reaches the end where current->next = NULL;

		delete (tempNode);
		tempNode = NULL;
	}
}

/*This function adds an bus stop to the list*/
bool List::add(ItemType item)
{
	Node* newNode = new Node; //Create new node
	newNode->item = item;
	newNode->next = NULL;

	if (firstNode == NULL) //Check whether it is empty
	{
		firstNode = newNode;
	}

	else //Traverse to last node 
	{
		Node* current = firstNode; //Current node replaced with firstNode;

		while (current->next != NULL) {
			current = current->next; //Keep traversing until current reaches the end where current->next = NULL;
		}

		current->next = newNode; //Add the new node
	}

	size++;
	return true;
}

/*Add at a specific index - Not used*/
bool List::add(int index, ItemType item)
{
	if (0 <= index <= size) {
		Node* newNode = new Node{ item, NULL };

		//Traverse the list
		if (index == 0) {
			newNode->next = firstNode;
			firstNode = newNode;
		}
		else {
			Node* currentNode = firstNode;
			for (int i = 0; i < index - 1; i++) {
				currentNode = currentNode->next;
			}
			newNode->next = currentNode->next;
			currentNode->next = newNode;
		}
		size++;
		return true;
	}
	return false;
}

/*Removal of bus stop from doubly-linked-list, hands in index to specify which bus stop to remove*/
void List::remove(int index)
{
	if (index >= 1 && index < size + 1)
	{
		Node* tempNode = firstNode; //Temporary Node

		if (index == 1)
		{
			firstNode = tempNode->next; //Sets the first node to firstNode->next hence removing the first node
			free(tempNode); //Delete unnecessary data;
		}
		else
		{
			int i;
			for (i = 0; i < index - 2; i++) //Traverse to node just before indexed node
			{
				tempNode = tempNode->next;
			}

			Node* tempNode2 = tempNode->next; //TempNode2 is now the node we want to delete
			tempNode->next = tempNode2->next; //TempNode.next is now tempNode2.next hence deleting tempNode2 (which is the node we want to delete)
			free(tempNode2); //Delete unnecessary data;
		}
		size--; //Size - 1
	}
}

ItemType List::getTop() { return firstNode; }

bool List::isEmpty()
{
	return size == 0;
}

int List::getLength()
{
	return size;
}

/*This function traverse the list and prints List items*/
void List::print(string type)
{
	if (type == "busRoute") {
		if (firstNode != NULL)
		{
			Node* current = firstNode; //copying firstNode to current
			int count = 1;
			while (current != NULL) //Keep printing every node until last one
			{
				BusStop* bs = ((BusStopDist*)current->item)->busStop;
				cout << count << "." << " " << bs->getBusStopName();
				double doubleDistance = ((BusStopDist*)current->item)->distance;
				//print double with 2 decimal places
				cout << " " << "(Distance: " << fixed << setprecision(2) << doubleDistance << "km)" << endl;
				current = current->next;
				count++;
			}
		}
		else {
			cout << "No Bus Stops registered under this route yet..." << '\n' << endl;
		}
	}
}
 
/*This function checks whether the the parameter busName exists inside the route

It will traverse the dictionary and check whether the name (which is the dictionary key) matches 
with the user input (busName)

If none of them matches, return NULL
else, return the BusStopDist object back*/

BusStopDist* List::exists(string busName, string type) {
	if (type == "BusRoute") {
		if (firstNode != NULL)
		{
			Node* current = firstNode; //copying firstNode to current
			while (current != NULL)
			{
				//Check whether bus stop name exists inside the route
				if (((BusStopDist*)current->item)->busStop->getBusStopName() == busName ) {
					return ((BusStopDist*)current->item);
				}
				current = current->next;
			}
		}
	}
	return NULL;
}

/*This function takes in a BusStopDist object and does a sorted insert based on distance in the BusStopDist* object

It will traverse the doubly linked list and compare distance, if it is bigger, it will keep traversing
until the next busStopDist object->item->distance is smaller and then insert it as a node.

*/

void List::SortedInsert(BusStopDist* bsd) {
	Node* current = firstNode;
	Node* previous = NULL;
	Node* newNode = new Node();
	newNode->item = bsd;
	newNode->next = NULL;

	while (current != NULL) {
		if (bsd->distance < ((BusStopDist*)current->item)->distance) {
			newNode->next = current;
			size++;
			break;
		}
		previous = current;
		current = current->next;
	}

	if (previous == NULL) {
		firstNode = newNode;
		size++;
	}
	else {
		previous->next = newNode;
		size++;
	}
}





