/*Name & Student ID:
Glenn - S10195629B
Team: 11
Class: P05*/

#pragma once

using namespace std;
#include<string>
#include "BusStop.h"
#include<iostream>

class Bus
{
private:
	string busNo;
	BusStop* nextStop;
	double distanceTravelled;
	string licensePlate;
	string model;
	int yearManufactured;

public:
	Bus(string bNo, BusStop* nSt, double dT, string lP, string m, int yMF); //Constructor
	void setBusNo(string b);
	string getBusNo();
	void setNextStop(BusStop* nS);
	BusStop* getNextStop();
	void setDistanceTravelled(double);
	double getDistanceTravelled();
	void setLicensePlate(string lp);
	string getLicensePlate();
	void setModel(string m);
	string getModel();
	void setYearManufactured(int yMF);
	int getYearManufactured();
};

