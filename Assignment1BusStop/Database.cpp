/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#include "Database.h"
#include <iostream>
#include "Bus.h"
#include "BusStop.h"
#include "List.h"
#include "BusRoute.h"
struct BusStopDist;

/*Filling of database*/
void Database::createDB(Dictionary* busDict, Dictionary* busRouteDict, Dictionary* busStopDict) {
	BusStop* busStop1 = new BusStop("Opp Blk 238", "Clementi Ave 3");
	BusStop* busStop2 = new BusStop("Blk 128", "Buona Vista 2");
	BusStop* busStop3 = new BusStop("Blk 123", "Toa Payoh Lorong 4");
	BusStop* busStop4 = new BusStop("Opp Blk 75", "Sembawang Road 37");
	busStopDict->add(busStop1->getBusStopName(), busStop1);
	busStopDict->add(busStop2->getBusStopName(), busStop2);
	busStopDict->add(busStop3->getBusStopName(), busStop3);
	busStopDict->add(busStop4->getBusStopName(), busStop4);


	Bus* bus1 = new Bus("196", busStop1, 0.0, "SHX470A", "Mercedes 3", 1999);
	Bus* bus2 = new Bus("184", busStop2, 3.0, "SGH367B", "Toyota", 1999);
	Bus* bus3 = new Bus("184", NULL, 5.0, "SFG985X", "Mercedes", 1999);
	busDict->add("SHX470A", bus1);
	busDict->add("SGH367B", bus2);
	busDict->add("SFG985X", bus3);

	//First Route
	BusRoute* busRoute1 = new BusRoute("196");
	BusStopDist* bsd = new BusStopDist();
	bsd->busStop = busStop1;
	bsd->distance = 3.4;
	BusStopDist* bsd1 = new BusStopDist();
	bsd1->busStop = busStop2;
	bsd1->distance = 6.2;
	BusStopDist* bsd2 = new BusStopDist();
	bsd2->busStop = busStop3;
	bsd2->distance = 7.2;
	BusStopDist* bsd3 = new BusStopDist();
	bsd3->busStop = busStop4;
	bsd3->distance = 8.3;
	busRoute1->addToBusRoute(bsd);
	busRoute1->addToBusRoute(bsd1);
	busRoute1->addToBusRoute(bsd2);
	busRoute1->addToBusRoute(bsd3);
	busRouteDict->add("196", busRoute1);

	//Second Route
	BusRoute* busRoute2 = new BusRoute("184");
	BusStopDist* bsd4 = new BusStopDist();
	bsd4->busStop = busStop2;
	bsd4->distance = 4.2;
	busRoute2->addToBusRoute(bsd4);
	busRoute2->addToBusRoute(bsd3);
	busRouteDict->add("184", busRoute2);
}