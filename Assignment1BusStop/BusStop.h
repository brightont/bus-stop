/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#pragma once
#include "List.h"

class List;
using namespace std;

class BusStop
{
private:
	string busStopName;
	string Location;
	List* listOfBusNo;

public:
	BusStop(string bStopName, string bStopLocation); //Constructor
	void setBusStopName(string b);
	string getBusStopName();
	void setLocation(string l);
	string getLocation();
	void addToBusList(string bNo);
};

