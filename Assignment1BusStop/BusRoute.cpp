/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#include "BusRoute.h"
#include "List.h"
using namespace std;

BusRoute::BusRoute(string bNo) {
	busNo = bNo;
	dLinkedListBusStop = new List;
}

void BusRoute::setBusNo(string b) { busNo = b; }

string BusRoute::getBusNo() { return busNo; }

/*Adds a BusStopDist Object to bus route by using add function from linked list*/
void BusRoute::addToBusRoute(BusStopDist* bStop) {
	dLinkedListBusStop->add(bStop);
}

List* BusRoute::getBusStopLinkedList() { 
	return dLinkedListBusStop;
}

void BusRoute::setMaxDistance(double md) { maxDistance = md; }

double BusRoute::getMaxDistance() { return maxDistance; }

/*Add to a BusStopDist object to bus route doubly-linked list - Not used*/
void BusRoute::addToBusRouteIndex(int index, BusStopDist* bStop) {
	dLinkedListBusStop->add(index,bStop);
}

