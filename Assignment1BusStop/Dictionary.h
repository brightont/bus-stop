/*Name & Student ID:
Glenn - S10195629B
Team: 11
Class: P05*/

#pragma once
#include<string>
#include<iostream>
using namespace std;

const int MAX_SIZE = 15;
typedef void* ItemType;
typedef string KeyType;

class Dictionary
{
private:
	struct Node
	{
		KeyType  key;   // search key
		ItemType item;	// data item
		Node* next;	// pointer pointing to next item
	};

	Node* items[MAX_SIZE];
	int  size;			// number of items in the Dictionary

public:
	// constructor
	Dictionary();

	// destructor
	~Dictionary();

	int hash(KeyType key);

	// add a new item with the specified key to the Dictionary
	bool add(KeyType newKey, ItemType newItem);

	// remove an item with the specified key in the Dictionary
	void remove(KeyType key);

	// get an item with the specified key in the Dictionary (retrieve)
	ItemType get(KeyType key);

	// check if the Dictionary is empty
	bool isEmpty();

	// check the size of the Dictionary
	int getLength();

	//------------------- Other useful functions -----------------
	// display the items in the Dictionary
	void print(string type);

	bool printBusRouteBS(string type, string bsName);

	int printCounter(string b, string type);
};

