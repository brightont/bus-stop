/*Name & Student ID:
Brighton Tan - S10185160A
Team: 11
Class: P05*/

#include "BusStop.h"

BusStop::BusStop(string bStopName, string bStopLocation){
	busStopName = bStopName;
	Location = bStopLocation;
}

void BusStop::setBusStopName(string b) { busStopName = b; }

string BusStop::getBusStopName(){return busStopName;}

void BusStop::setLocation(string l) { Location = l; }

string BusStop::getLocation() { return Location; }

/*Calls add function from doubly linked list and adds to the list of BusNo - Not used */
void BusStop::addToBusList(string bNo) {
	listOfBusNo->add(&bNo);
}