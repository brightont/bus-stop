/*Name & Student ID:
Glenn - S10195629B
Team: 11
Class: P05*/

#include "Dictionary.h"
#include "Bus.h"
#include "List.h"
using namespace std;

// constructor
Dictionary::Dictionary() {
	for (int i = 0; i < MAX_SIZE; i++)
	{
		items[i] = NULL;
	}
	size = 0;
}

// destructor
Dictionary::~Dictionary() {
	for (int i = 0; i < MAX_SIZE; i++) {
		if (items[i] != NULL) {
			Node* current = items[i];
			while (current != NULL) {
				Node* temp;
				temp = current;
				current = (current->next);
				delete temp;
				temp = NULL;
			}
			items[i] = NULL;
			size = 0;
		}
	}
}

/*Function for calculating numerical value of alphabets for hash function
  Also returns value for numerical inputs*/
int charvalue(char c)
{
	if (isalpha(c))
	{
		if (isupper(c))
			return (int)c - (int)'A';
		else
			return (int)c - (int)'a' + 26;
	}
	else if (isdigit(c)) {
		return atoi(&c) + 53;
	}
}
/*Hash Function for universal dictionary type
  
  This hash function is used for storing 3 different object types for the project.
  Since this hash function allows the mixture of alphabets and numbers, it is
  flexible in terms of application for this project.
*/
int Dictionary::hash(KeyType key) {
	int index = charvalue(key[0]);
	for (int i = 0; i < key.length(); i++)
	{
		if (charvalue(key[i]) < 0) {
			continue;
		}
		index = (index * 62 + charvalue(key[i])) % MAX_SIZE; //Multiply by number of states
	}
	return (index);
}

// add a new item with the specified key to the Dictionary
bool Dictionary::add(KeyType newKey, ItemType newItem) {
	int index = hash(newKey);
	if (items[index] == NULL)
	{
		Node* newNode = new Node();
		newNode->key = newKey;
		newNode->item = newItem;
		newNode->next = NULL;
		items[index] = newNode;
	}
	else {
		Node* current = items[index];
		if (current->key == newKey)
		{
			return false;
		}
		while (current->next != NULL)
		{
			if (current->next->key == newKey)
			{
				return false;
			}
			current = current->next;
		}
		Node* newNode = new Node;
		newNode->key = newKey;
		newNode->item = newItem;
		newNode->next = NULL;
		current->next = newNode;
	}
	size++;
	return true;
}

// remove an item with the specified key in the Dictionary
void Dictionary::remove(KeyType key) {
	int index = hash(key);
	if (items[index] != NULL) {
		Node* current = items[index];
		Node* previous = items[index];
		while (current != NULL) {
			if (current->key == key)
			{
				if (current == items[index]) {
					items[index] = current->next;
				}
				else {
					previous->next = current->next;
				}
				delete current;
				current = NULL;
				size--;
				return;
			}
			current = current->next;
			previous = current;
		}
	}
}

// get an item with the specified key in the Dictionary (retrieve)
ItemType Dictionary::get(KeyType key) {
	int index = hash(key);
	if (items[index] != NULL) {
		Node* current = items[index];
		while (current != NULL) {
			if (current->key == key)
			{
				return current->item;
			}
			current = current->next;
		}
	}
	return NULL;
}

// check if the Dictionary is empty
bool Dictionary::isEmpty() {
	return size == 0;
}

// check the size of the Dictionary
int Dictionary::getLength() {
	return size;
}

//------------------- Other useful functions -----------------

/* Customized Print Function for Dictionary
* 
   This customized function checks for a type input.

   Upon validating the type input, different actions will be taken.
   This is necessary as the dictionary is initially set to a void ItemType.
   
   Therefore, upcasting is necessary for displaying the features of the item retrieved.
   This requires a check for specific types of objects to be retrieved.
*/
void Dictionary::print(string type) {
	if (type == "BusRoute") {
		for (int i = 0; i < MAX_SIZE; i++) {
			if (items[i] != NULL) {
				Node* current = items[i];
				while (current != NULL) {
					cout << ((BusRoute*)current->item)->getBusNo() << endl;
					current = current->next;
				}
			}
		}
		cout << endl;
	}
	else {
		for (int i = 0; i < MAX_SIZE; i++) {
			cout << "[" << i << "] \t";
			if (items[i] != NULL) {
				Node* current = items[i];
				while (current != NULL) {
					cout << current->key << ":" << current->item << "->";
					current = current->next;
				}
			}
			cout << "NULL" << endl;
		}
		cout << endl;
	}
}

/*Custom-Made function meant for Main Function 9: Display buses on a particular Bus Route.
  
  This function is a counter function which increases whenever there is a bus currently servicing
  on a specific route.

  It accepts 2 string parameters, the first being the bus number and the second being the type.
  The type string is a validation to ensure the right actions are called.

  This function traverses through the bus dictionary to find the specific bus number specified.
  If the distanceTravelled attribute is greater than 0, the counter will increment by 1.

  At the end, the counter will be displayed as the final output.
*/
int Dictionary::printCounter(string b, string type) {
	int count = 0;
	if (type == "busDict")
	{
		for (int i = 0; i < MAX_SIZE; i++) {
			if (items[i] != NULL) {
				Node* current = items[i];
				while (current != NULL) {
					Bus* bus = (Bus*)(current->item);
					if (bus->getBusNo() == b)
					{
						if (bus->getDistanceTravelled() > 0)
						{
							count++;
						}
					}
					current = current->next;
				}
			}
		}

		return count;
	}
}

/*Custom-Made Function meant for Main Function 10: Display routes that contain a specific bus stop
  
  Function accepts 2 strings as parameter, the first being the type and the second being the bus stop name.

  The function recurs through the Bus Route dictionary and retrieves every bus route's linked list attribute.
  The linked list attribute is traversed and it checks if the specified bus stop exists within the list.
  
  If the bus stop is found, the route number will be displayed as final output.
*/
bool Dictionary::printBusRouteBS(string type, string bsName) {
	int count = 1;
	if (type == "BusRoute") {
		for (int i = 0; i < MAX_SIZE; i++) {
			if (items[i] != NULL) {
				Node* current = items[i];
				while (current != NULL) {
					BusRoute* currentBr = (BusRoute*)(current->item);
					List* brList = currentBr->getBusStopLinkedList();
					BusStopDist* bsD = NULL;
					bsD = brList->exists(bsName, "BusRoute");
					if (bsD != NULL) {
						cout << count << ". " << "Route " << currentBr->getBusNo() << endl;
						count++;
						current = current->next;
					}
					else {
						current = current->next;
					}
				}
			}
		}
	}
	if (count == 1) {
		return false;
	}
	else {
		cout << endl;
		return true;
	}
}